/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Date;

/**
 *
 * @author Alexander
 */
public class Productos {
    private int Id;
    private int Codigo;
    private String Nombre;
    private String ProveedorPro;
    private int Stock;
    private Double Precio;
    private Date Fecha;

    public Productos() {
    }

    public Productos(int Id, int Codigo, String Nombre, String ProveedorPro, int Stock, Double Precio, Date Fecha) {
        this.Id = Id;
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.ProveedorPro = ProveedorPro;
        this.Stock = Stock;
        this.Precio = Precio;
        this.Fecha = Fecha;
    }

    
    
    
    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getProveedorPro() {
        return ProveedorPro;
    }

    public void setProveedorPro(String ProveedorPro) {
        this.ProveedorPro = ProveedorPro;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double Precio) {
        this.Precio = Precio;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

}