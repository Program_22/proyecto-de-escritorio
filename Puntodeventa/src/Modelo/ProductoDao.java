/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author Alexander
 */
public class ProductoDao {

    Conexion pro = new Conexion();
    Connection prod;
    PreparedStatement produ;
    ResultSet prodt;

    public boolean RegistrarProducto(Producto product) {
        String sql = "INSERT INTO productos(codigo,nombre,proveedor,stock,precio) VALUES (?,?,?,?,?)";
        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            produ.setInt(1, product.getCodigo());
            produ.setString(2, product.getNombre());
            produ.setString(3, product.getProveedor());
            produ.setInt(4, product.getStock());
            produ.setDouble(5, product.getPrecio());

            produ.execute();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {

            try {
                prod.close();

            } catch (SQLException e) {
                System.out.println(e.toString());

            }
        }

    }

    public List ListarProducto() {
        List<Producto> ListaPro = new ArrayList();
        String sql = "SELECT* FROM productos";
        try {

            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            prodt = produ.executeQuery();
            while (prodt.next()) {
                Producto product = new Producto();
                product.setId(prodt.getInt("id"));
                product.setCodigo(prodt.getInt("codigo"));
                product.setNombre(prodt.getString("nombre"));
                product.setProveedor(prodt.getString("proveedor"));
                product.setStock(prodt.getInt("stock"));
                product.setPrecio(prodt.getDouble("precio"));

                ListaPro.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return ListaPro;

    }

    public boolean EliminarProducto(int id) {
        String sql = "DELETE FROM productos where id= ?";
        try {
            produ = prod.prepareStatement(sql);
            produ.setInt(1, id);

            produ.execute();
            return true;

        } catch (SQLException e) {

            System.out.println(e.toString());
            return false;

        } finally {

            try {

                prod.close();
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }

    }

    public boolean ModificarProducto(Producto product) {
        String sql = "UPDATE productos SET codigo=?,nombre=?,proveedor=?,stock=? ,precio=? WHERE id=?";
        try {
            produ = prod.prepareStatement(sql);
            produ.setInt(1, product.getCodigo());
            produ.setString(2, product.getNombre());
            produ.setString(3, product.getProveedor());
            produ.setInt(4, product.getStock());
            produ.setDouble(5, product.getPrecio());
            produ.setInt(6, product.getId());
            produ.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {

                prod.close();
            } catch (SQLException e) {

                System.out.println(e.toString());
            }
        }
    }

    public void ConsultarProveedor(JComboBox proveedor) {
        String sql = "SELECT nombre FROM proveedor";

        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            prodt = produ.executeQuery();
            while (prodt.next()) {
                proveedor.addItem(prodt.getString("nombre"));

            }

        } catch (SQLException e) {
            System.out.println(e.toString());

        }

    }
     public Producto BuscarPro(String cod) {
        Producto producto = new Producto();
        String sql = "SELECT *FROM productos WHERE codigo= ?";
        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            produ.setString(1, cod);
            prodt = produ.executeQuery();
            if (prodt.next()) {
                producto.setNombre(prodt.getString("nombre"));
                producto.setPrecio(prodt.getDouble("precio"));
                producto.setStock(prodt.getInt("stock"));
            }

        } catch (SQLException e) {

            System.out.println(e.toString());
        }

        return producto;
    }

}
