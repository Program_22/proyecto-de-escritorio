/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class ProveedorDao {
    Conexion pv=new Conexion();
    Connection prv;
    PreparedStatement pr;
    ResultSet rp;
    public boolean RegistrarProveedor(Proveedor provee){
   String sql="INSERT INTO proveedor(ruc,nombre,telefono,direccion,razon) VALUES (?,?,?,?,?)";     
        try{
   prv=pv.getConnection();
   pr=prv.prepareStatement(sql);
   pr.setInt(1,provee.getRuc());
   pr.setString(2,provee.getNombre());
   pr.setInt(3, provee.getTelefono());
   pr.setString(4,provee.getDireccion());
   pr.setString(5, provee.getRazon());
   pr.execute();
  return true; 
        }catch (SQLException e){
     JOptionPane.showMessageDialog(null,e.toString()) ;      
         return false;   
            
        }finally{
           try{
              prv.close();
               
           } catch(SQLException e){
               System.out.println(e.toString());
           }
            
            
        }
        
    
    }
   public List ListarProveedor(){
       List<Proveedor> ListaPv=new ArrayList();
       String sql="SELECT* FROM proveedor";
       try{
           
           prv=pv.getConnection();
           pr=prv.prepareStatement(sql); 
           rp=pr.executeQuery();
           while(rp.next()){
        Proveedor prov=new Proveedor(); 
         prov.setId(rp.getInt("id"));
            prov.setRuc(rp.getInt("ruc"));
            prov.setNombre(rp.getString("nombre"));
            prov.setTelefono(rp.getInt("telefono"));
            prov.setDireccion(rp.getString("direccion"));
            prov.setRazon(rp.getString("razon"));
            ListaPv.add(prov);
            
           }
       }catch(SQLException e){
                      System.out.println(e.toString());
       }
           
             return ListaPv;
       
   }
   
       public boolean EliminarProveedor(int id) {
        String sql = "DELETE FROM proveedor where id= ?";
        try {
            pr = prv.prepareStatement(sql);
            pr.setInt(1, id);
            pr.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                prv.close();
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
       
     
    public boolean ModificarProveedor(Proveedor provee) {
        String sql = "UPDATE proveedor SET ruc=?,nombre=?,telefono=?,direccion=? ,razon=? WHERE id=?";
        try {
            pr = prv.prepareStatement(sql);
            pr.setInt(1, provee.getRuc());
            pr.setString(2, provee.getNombre());
            pr.setInt(3, provee.getTelefono());
            pr.setString(4, provee.getDireccion());
            pr.setString(5, provee.getRazon());
            pr.setInt(6, provee.getId());
            pr.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                prv.close();

            } catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

    }
  
       

}
