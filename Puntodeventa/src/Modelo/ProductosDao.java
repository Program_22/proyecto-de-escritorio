/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author Alexander
 */
public class ProductosDao {

    Conexion pro = new Conexion();
    Connection prod;
    PreparedStatement produ;
    ResultSet prodt;

    public boolean RegistrarProducto(Productos product) {
        String sql = "INSERT INTO productos(codigo,nombre,proveedor,stock,precio) VALUES (?,?,?,?,?)";
        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            produ.setInt(1, product.getCodigo());
            produ.setString(2, product.getNombre());
            produ.setString(3, product.getProveedorPro());
            produ.setInt(4, product.getStock());
            produ.setDouble(5, product.getPrecio());

            produ.execute();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {

            try {
                prod.close();

            } catch (SQLException e) {
                System.out.println(e.toString());

            }
        }

    }

    public List ListarProductos() {
        List<Productos> ListaPro = new ArrayList();
        String sql = "SELECT* FROM productos";
        try {

            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            prodt = produ.executeQuery();
            while (prodt.next()) {
                Productos product = new Productos();
                product.setId(prodt.getInt("id"));
                product.setCodigo(prodt.getInt("codigo"));
                product.setNombre(prodt.getString("nombre"));
                product.setProveedorPro(prodt.getString("proveedor"));
                product.setStock(prodt.getInt("stock"));
                product.setPrecio(prodt.getDouble("precio"));

                ListaPro.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return ListaPro;

    }

    public boolean EliminarProductos(int id) {
        String sql = "DELETE FROM productos where id= ?";
        try {
            produ = prod.prepareStatement(sql);
            produ.setInt(1, id);

            produ.execute();
            return true;

        } catch (SQLException e) {

            System.out.println(e.toString());
            return false;

        } finally {

            try {

                prod.close();
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }

    }

    public boolean ModificarProducto(Productos product) {
        String sql = "UPDATE productos SET codigo=?,nombre=?,proveedor=?,stock=? ,precio=? WHERE id=?";
        try {
            produ = prod.prepareStatement(sql);
            produ.setInt(1, product.getCodigo());
            produ.setString(2, product.getNombre());
            produ.setString(3, product.getProveedorPro());
            produ.setInt(4, product.getStock());
            produ.setDouble(5, product.getPrecio());
            produ.setInt(6, product.getId());
            produ.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        } finally {
            try {

                prod.close();
            } catch (SQLException e) {

                System.out.println(e.toString());
            }
        }
    }

    public void ConsultarProveedor(JComboBox proveedor) {
        String sql = "SELECT nombre FROM proveedor";

        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            prodt = produ.executeQuery();
            while (prodt.next()) {
                proveedor.addItem(prodt.getString("nombre"));

            }

        } catch (SQLException e) {
            System.out.println(e.toString());

        }

    }
     public Productos BuscarPro(String cod) {
        Productos producto = new Productos();
        String sql = "SELECT *FROM productos WHERE codigo= ?";
        try {
            prod = pro.getConnection();
            produ = prod.prepareStatement(sql);
            produ.setString(1, cod);
            prodt = produ.executeQuery();
            if (prodt.next()) {
                producto.setNombre(prodt.getString("nombre"));
                producto.setPrecio(prodt.getDouble("precio"));
                producto.setStock(prodt.getInt("stock"));
            }

        } catch (SQLException e) {

            System.out.println(e.toString());
        }

        return producto;
    }
 public Config BuscarDatos(){
        Config conf = new Config();
        String sql = "SELECT * FROM config";
        try {
           prod = pro.getConnection();
             produ = prod.prepareStatement(sql);
            prodt = produ.executeQuery();
            if (prodt.next()) {
                conf.setId(prodt.getInt("id"));
                conf.setRuc(prodt.getString("ruc"));
                conf.setNombre(prodt.getString("nombre"));
                conf.setTelefono(prodt.getString("telefono"));
                conf.setDireccion(prodt.getString("direccion"));
                conf.setMensaje(prodt.getString("mensaje"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return conf;
    }
 
   public Proveedor BuscarProveedor(String nombre){
        Proveedor pr = new Proveedor();
        String sql = "SELECT * FROM proveedor WHERE nombre = ?";
        try {
              prod = pro.getConnection();
             produ = prod.prepareStatement(sql);
            produ.setString(1, nombre);
            prodt = produ.executeQuery();
            if (prodt.next()) {
                pr.setId(prodt.getInt("id"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return pr;
    }
 public boolean ModificarDatos(Config conf){
       String sql = "UPDATE config SET ruc=?, nombre=?, telefono=?, direccion=?, mensaje=? WHERE id=?";
       try {
           produ= prod.prepareStatement(sql);
           produ.setString(1, conf.getRuc());
           produ.setString(2, conf.getNombre());
           produ.setString(3, conf.getTelefono());
           produ.setString(4, conf.getDireccion());
           produ.setString(5, conf.getMensaje());
           produ.setInt(6, conf.getId());
           produ.execute();
           return true;
       } catch (SQLException e) {
           System.out.println(e.toString());
           return false;
       }finally{
           try {
               prod.close();
           } catch (SQLException e) {
               System.out.println(e.toString());
           }
       }
   }
}
